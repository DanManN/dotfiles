import sys
import numpy as np
import matplotlib.pyplot as plt

sizeIn = input("What square size (in pixels) do you want the fractal to be?\n")
size = 1000
try:
    size = int(sizeIn)
except Exception:
    print(
        "\033[1;31mIncorrect type for size. Using default value: %d\033[1;m" % size,
        file=sys.stderr
    )

maxIterationsIn = input("How many iterations do you want?\n")
maxIterations = 60
try:
    maxIterations = int(maxIterationsIn)
except Exception:
    print(
        "\033[1;31mIncorrect type for maxIterations. Using default value: %d\033[1;m" %
        maxIterations,
        file=sys.stderr
    )

rIn = input("Real part of complex constant? (I suggest between about -2.0, 1.0)\n")
r = -1.0
try:
    r = float(rIn)
except Exception:
    print(
        "\033[1;31mIncorrect type for real component. Using default value: %f\033[1;m" % r,
        file=sys.stderr
    )

iIn = input("Imaginary part of complex constant? (I suggest between about -1.5 and 1.5)\n")
i = 0.0
try:
    i = float(iIn)
except Exception:
    print(
        '''\033[1;31mIncorrect type for imaginary component.
        Using default value: %f\033[1;m''' % i,
        file=sys.stderr
    )

c = np.complex256(np.complex(r, i))

iterations = np.zeros((size, size))
z = np.fromfunction(
    lambda x, y: np.complex256((4 * y / size - 2) + (4 * x / size - 2) * (0 + 1j)),
    (size, size),
    dtype=np.complex256
)

for x in range(maxIterations):
    good = np.abs(z) < 2
    z[good] = z[good]**2 + c
    iterations[good] += 1
    temp = x / (maxIterations - 1)
    sys.stdout.write("\r")
    sys.stdout.write("Loading...<%-30s> %d%%" % ('=' * (int(30 * temp)), int(100 * temp)))
    sys.stdout.flush()

print()

fig = plt.figure()
plt.imshow(iterations, interpolation='none')
plt.colorbar()
ax = fig.add_subplot(1, 1, 1)
ax.imshow(iterations)
ax.set_xticks([0, size / 4, size / 2, size * 3 / 4, size])
ax.set_xticklabels([-2, -1, 0, 1, 2])
ax.set_yticks([0, size / 4, size / 2, size * 3 / 4, size])
ax.set_yticklabels([2, 1, 0, -1, -2])
plt.show()
