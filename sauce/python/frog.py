mem = {}


def num_jumps_TD(p):
    d = p + 1
    if d == 1:
        return 1

    if p not in mem:
        mem[p] = 1 + (1 / d) * sum([num_jumps_TD(x) for x in range(0, p)])

    return mem[p]


def num_jumps_BU(p):
    d = p + 1
    arr = [1]
    for x in range(2, d + 1):
        arr.append(1 + (1 / x) * sum(arr[:x - 1]))
    return arr[-1]


def num_jumps(p):
    d = p + 1
    ans = 1
    for x in range(2, d + 1):
        ans += 1 / x
    return ans
