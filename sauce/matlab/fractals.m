close all;clear all;clc
size = 2000;
a = linspace(-2,2,size);
x = meshgrid(a,a);
y = rot90(x);
z = x+y*j;
it = zeros(size);
%c = 2*rand-1+(2*rand-1)*j;
c = -0.2967 + 0.6617i;%z2
%c = 0.420+0.420j;%z2
%c = 0.8311 - 0.2004i;%z2
%c = -0.4803 + 0.6001i;%z2
%c = 0.6814 - 0.4914i;%cos2
h = waitbar(0,'Loading...');
max = 20;
tic
for n = 1:max
	%formula:
	z(abs(z) < 2) = z(abs(z) < 2).^2 + c;
	it(abs(z) < 2) = it(abs(z) < 2)+1;
	waitbar(n/max,h)
end
toc
close(h)
figure
map = prism(max);
colormap(map);
pcolor(it);
shading flat;
axis('square','equal','off');
figure
colormap(map);
W= exp(-abs(z)./max);
pcolor(W);
shading flat;
axis('square','equal','off');
c
