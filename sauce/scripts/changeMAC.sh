#!/usr/bin/env bash
DEV="$1"
ADDR="$2"
ip link set dev $DEV down
ip link set dev $DEV address $ADDR
ip link set dev $DEV up
