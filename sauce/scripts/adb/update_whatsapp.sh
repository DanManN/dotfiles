#!/usr/bin/env bash
# get local version
oldVer=$(ls whatsapp_*_.apk -v1 | tail -n 1 | cut -d_ -f2)
# get latest version
curVer=$(curl -so - https://www.whatsapp.com/android | grep -oP 'Version \d+.\d+.\d+.\d+' | cut -d' ' -f2)
# Check if the server version is newer
newVer=$(echo -e "$oldVer\n$curVer" | sort -n | tail -n 1)
# Download the newer version
if [ "$curVer" = "$newVer" ] && [ "$oldVer" != "$curVer" ]
then
	curl -Lo whatsapp_${newVer}_.apk http://www.whatsapp.com/android/current/WhatsApp.apk && adb install whatsapp_${newVer}_.apk || echo "Failed to install."
else
	echo "The newest version is already downloaded. Skipping Install."
fi
# Delete old file
rm -f $(ls whatsapp_*_.apk | grep -v $newVer)
