#!/usr/bin/env bash
fdroidcl update
fdroidcl install \
	im.vector.app \
	net.osmand.plus \
	at.bitfire.davdroid \
	at.bitfire.icsdroid \
	com.nextcloud.client \
	org.kde.kdeconnect_tp \
	org.telegram.messenger \
	com.kunzisoft.keepass.libre \
	de.markusfisch.android.binaryeye \
	# com.github.catfriend1.syncthingandroid \
	# org.thoughtcrime.securesms
