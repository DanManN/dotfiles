#!/usr/bin/env bash
cat /etc/pacman.d/mirrorlist.pacnew | tr -d '#' | grep '^Server = https' | rankmirrors -n 6 - > /etc/pacman.d/mirrorlist
cat /etc/pacman.d/chaotic-mirrorlist.pacnew | grep '^Server' | tr -d '#' | rankmirrors -n 6 -r chaotic-aur - > /etc/pacman.d/chaotic-mirrorlist
cat /etc/pacman.d/arch4edu-mirrorlist.pacnew | tr -d '#' | grep '^Server' | rankmirrors -n 6 - > /etc/pacman.d/arch4edu-mirrorlist
