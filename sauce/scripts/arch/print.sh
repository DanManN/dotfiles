#!/usr/bin/env bash
IP="$1"
FILE="$2"
NUM="$3"

if [ -z "$FILE" ]; then
    echo "No file specified!"
    exit
fi
pdf2ps "$FILE" temp.ps
for x in $(seq 1 $NUM)
do
	ncat $IP 9100 -v < temp.ps
done
rm temp.ps
