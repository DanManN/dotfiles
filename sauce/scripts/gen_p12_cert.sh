#!/usr/bin/env bash
read -s PASS
openssl genrsa -out p_key.pem 4096
openssl req -batch -new -x509 -key p_key.pem -keyform PEM -sha256 -outform PEM -out serve_cert.pem
openssl pkcs12 -export -in serve_cert.pem -inkey p_key.pem -out smskeystore.p12 -name smskeystore -password pass:$PASS
