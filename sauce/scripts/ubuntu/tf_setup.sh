#!/usr/bin/env bash
sudo apt update
sudo apt upgrade

# Install
sudo apt install -y python-dev python-pip python-virtualenv python3-dev python3-pip python3-virtualenv virtualenv

# Install TensorRT. Requires that libcudnn7 is installed.
sudo apt install -y --no-install-recommends libnvinfer6 libnvinfer-dev

cd ./tf_test
virtualenv -p python3 ./venv
source ./venv/bin/activate
pip install tensorflow-gpu
python tf_img_classif.py
deactivate
cd ..
