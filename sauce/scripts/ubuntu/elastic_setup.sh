#!/usr/bin/env bash
sudo install apt-transport-https
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo gpg --dearmor -o /usr/share/keyrings/elasticsearch-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/elasticsearch-keyring.gpg] https://artifacts.elastic.co/packages/8.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-8.x.list
sudo apt update
sudo apt install elasticsearch
sudo systemctl daemon-reload
sudo systemctl enable elasticsearch.service

# nextcloud search servlet address:
# http://elastic:<password>@<ip-address>:9200
# **remember to disable ssl in /etc/elasticsearch/elasticsearch.yml**

# useful commands for next steps:
# sudo systemctl start elasticsearch.service
# curl -u elastic -X PUT "http://localhost:9200/my-index?pretty"
# or
# curl --cacert /etc/elasticsearch/certs/http_ca.crt -u elastic -X PUT "https://localhost:9200/my-index?pretty"

# curl -u elastic -X PUT "http://localhost:9200/_ingest/pipeline/attachment" -H 'Content-Type: application/json' -d '{
#   "description" : "Extract attachment information",
#   "processors" : [
#     {
#       "attachment" : {
#         "field" : "content",
#         "indexed_chars" : -1
#       }
#     }
#   ]
# }'

# docker exec -u www-data nextcloud-apache php /var/www/html/occ fulltextsearch:test
# docker exec -u www-data nextcloud-apache php /var/www/html/occ fulltextsearch:index
