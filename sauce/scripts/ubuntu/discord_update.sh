#!/usr/bin/env bash

echo Installed: $([ -f /usr/share/discord/resources/build_info.json ] && cat /usr/share/discord/resources/build_info.json | grep -oP '\d+.\d+.\d+' || echo 'None')
echo Available: $(wget --spider "https://discordapp.com/api/download?platform=linux&format=deb" 2>&1 | grep Location | sed -e 's/.*linux\/\(.*\)\/.*/\1/')

REPLY=n
echo -n "Download and install? (y/N)"
read REPLY
if [ "$REPLY" = y ] ; then
	wget -c -O discord.deb "https://discordapp.com/api/download?platform=linux&format=deb"
	sudo apt install ./discord.deb
	rm discord.deb
fi
