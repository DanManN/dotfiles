# sudo apt-key adv --keyserver keys.gnupg.net --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE || sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE

# # Install
# case "$(lsb_release -sr)" in
#         "16.04")
#                 sudo add-apt-repository "deb http://realsense-hw-public.s3.amazonaws.com/Debian/apt-repo xenial main" -u
#                 ;;
#         "18.04")
#                 sudo add-apt-repository "deb http://realsense-hw-public.s3.amazonaws.com/Debian/apt-repo bionic main" -u
#                 ;;
#         "20.04")
#                 sudo add-apt-repository "deb http://realsense-hw-public.s3.amazonaws.com/Debian/apt-repo focal main" -u
#                 ;;
# esac

# sudo apt update

# # main packages
# sudo apt-get install -y librealsense2-dkms librealsense2-utils
# # dev packages
# sudo apt-get install -y librealsense2-dev librealsense2-dbg

### NEW ###
sudo apt update
sudo apt install libglfw3-dev libgl1-mesa-dev libglu1-mesa-dev libusb-1.0-0-dev
git clone https://github.com/IntelRealSense/librealsense
mkdir -p librealsense/build && cd librealsense/build
cmake .. -DFORCE_RSUSB_BACKEND=true -DCMAKE_BUILD_TYPE=release
make -j$(nproc)
# sudo make install
