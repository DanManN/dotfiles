#!/usr/bin/env bash

echo Installed: $(singularity --version)
VERSION="$(wget --spider https://github.com/apptainer/singularity/releases/latest 2>&1 | grep -oP 'v\d+.\d+.\d+' | head -n 1)"
echo Available: $VERSION

REPLY=n
echo -n "Download and install? (y/N)"
read REPLY
if [ "$REPLY" = y ] ; then
	wget -c "https://github.com/apptainer/singularity/releases/download/$VERSION/singularity-container_${VERSION:1}_amd64.deb"
	sudo apt install "./singularity-container_${VERSION:1}_amd64.deb"
	rm "singularity-container_${VERSION:1}_amd64.deb"
fi
