#!/usr/bin/env bash

echo Iosevka Version: $(wget --spider https://github.com/ryanoasis/nerd-fonts/releases/latest/download/Iosevka.zip 2>&1 | grep -oP "v\d\.\d\.\d" | head -n 1)
echo Victor Version: $(wget --spider https://github.com/ryanoasis/nerd-fonts/releases/latest/download/VictorMono.zip 2>&1 | grep -oP "v\d\.\d\.\d" | head -n 1)

REPLY=n
echo -n "Download and install? (y/N)"
read REPLY
if [ "$REPLY" = y ] ; then
	wget -c https://github.com/ryanoasis/nerd-fonts/releases/latest/download/Iosevka.zip
	wget -c https://github.com/ryanoasis/nerd-fonts/releases/latest/download/VictorMono.zip
	unzip Iosevka.zip -d ~/.local/share/fonts/
	unzip VictorMono.zip -d ~/.local/share/fonts/
	fc-cache -fv
fi
