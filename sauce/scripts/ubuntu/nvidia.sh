#!/usr/bin/env bash
CUDA_VER="10.2.89-1"
CUDA_VER_S="10-2"
CUDNN_VER="7"

case "$(lsb_release -sr)" in
"18.04")
	UBUNTU_REL="ubuntu1804"
	;;
"16.04")
	UBUNTU_REL="ubuntu1604"
	;;
esac

# Repos
wget https://developer.download.nvidia.com/compute/cuda/repos/$UBUNTU_REL/x86_64/cuda-repo-${UBUNTU_REL}_${CUDA_VER}_amd64.deb
sudo apt install -y ./cuda-repo-${UBUNTU_REL}_${CUDA_VER}_amd64.deb
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/$UBUNTU_REL/x86_64/7fa2af80.pub
wget http://developer.download.nvidia.com/compute/machine-learning/repos/$UBUNTU_REL/x86_64/nvidia-machine-learning-repo-${UBUNTU_REL}_1.0.0-1_amd64.deb
sudo apt install -y ./nvidia-machine-learning-repo-${UBUNTU_REL}_1.0.0-1_amd64.deb
sudo apt update
sudo apt upgrade

# Install
sudo apt install -y \
    cuda-$CUDA_VER_S \
    libcudnn$CUDNN_VER  \
    libcudnn$CUDNN_VER-dev

sudo apt autoremove
