#!/usr/bin/env bash

case "$1" in
    ''|*[!0-9]*) exit ;;
    *) VER=-"$1" ;;
esac

wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | sudo apt-key add -
# Fingerprint: 6084 F3CF 814B 57C1 CF12 EFD5 15CF 4D18 AF4F 7421
sudo add-apt-repository "deb http://apt.llvm.org/bionic/ llvm-toolchain-$(lsb_release -cs)$VER main"

sudo apt update
sudo apt install -y clang$VER clang-format$VER clang-tools$VER clangd$VER
for x in $(ls /usr/bin | grep "clang.*\\$VER\$"); do
	ln -sf /usr/bin/"$x" $HOME/.local/bin/"${x%$VER}" 2> /dev/null || true
done
