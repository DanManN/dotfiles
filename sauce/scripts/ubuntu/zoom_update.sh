#!/usr/bin/env bash

echo Installed: $([ -f /opt/zoom/version.txt ] && cat /opt/zoom/version.txt || echo 'None')
echo Available: $(wget --spider https://zoom.us/client/latest/zoom_amd64.deb 2>&1 | grep Location | sed -e 's/.*prod\/\(.*\)\/.*/\1/')

REPLY=n
echo -n "Download and install? (y/N)"
read REPLY
if [ "$REPLY" = y ] ; then
	wget -c https://zoom.us/client/latest/zoom_amd64.deb
	sudo apt install ./zoom_amd64.deb
	rm zoom_amd64.deb
fi
