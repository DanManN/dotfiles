#!/usr/bin/env bash
case "$(lsb_release -sr)" in
"18.04")
	UBUNTU_REL="1804"
	;;
"16.04")
	UBUNTU_REL="1604"
	;;
esac
CUDNN_DOC="libcudnn7-doc_7.6.5.32-1+cuda10.2_amd64_$UBUNTU_REL.deb"
# download correct file from https://developer.nvidia.com/rdp/cudnn-download

sudo apt install ./$CUDNN_DOC

cp -r /usr/src/cudnn_samples_v7/ $HOME
cd  $HOME/cudnn_samples_v7/mnistCUDNN
make clean && make
./mnistCUDNN
