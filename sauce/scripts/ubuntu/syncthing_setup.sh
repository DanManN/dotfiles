#!/usr/bin/env bash
sudo apt update
sudo install syncthing
# useradd -mG nextcloud,www-data syncthing
cp "$HOME/slash/etc/systemd/system/syncthing.service" /etc/systemd/system/
systemctl daemon-reload
