#!/usr/bin/env bash
docker run -d \
    --name wgui \
    -e LOGIN_PAGE=1 \
    -e BIND_ADDRESS=0.0.0.0:5000 \
    --net=host \
    -v /path/data:/db \
    -v /etc/wireguard:/etc/wireguard \
    --restart unless-stopped \
    --privileged \
    jarylc/wireguard-ui
