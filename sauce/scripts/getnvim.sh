#!/usr/bin/env bash
curl -L https://github.com/neovim/neovim/releases/download/stable/nvim.appimage -o ~/.local/bin/nvim
ln -s nvim ~/.local/bin/vim
ln -s nvim ~/.local/bin/vi
