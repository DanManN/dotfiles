#!/usr/bin/env bash

IP_LIST="$(cat $HOME/.ssh/config | grep '\.' | grep -v File | cut -d' ' -f2)"
if [[ -f $HOME/.ssh/known_hosts ]]; then
	for IP in $IP_LIST; do
		ssh-keygen -R $IP
	done
fi

ssh-keyscan -H $IP_LIST >> $HOME/.ssh/known_hosts
