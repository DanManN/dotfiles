#!/usr/bin/env bash

ESSID="Blah"
USER="Blah"
PASS="Blah"

interface=$1 status=$2
case $status in
  up)
    if nmcli -t -f active,ssid dev wifi | grep -qs "yes:$ESSID"; then

        # Check if logged in already
        redir=$(curl -s http://detectportal.firefox.com/success.txt)
        [[ $redir != "success" ]] || { echo "Not running script. Already logged in." && exit 0; }

        # If not, log in!
        echo "Running login script for $ESSID wifi."
        result=$(curl -s "https://captiveportal-login.cooper.edu/cgi-bin/login?user=$USER&password=$PASS&cmd=authenticate")

    fi
    ;;
  down)
    if nmcli -t -f active,ssid dev wifi | grep -qs "yes:$ESSID"; then

        # Log out!
        echo "Running logout script for $ESSID wifi."
        result=$(curl -s "https://captiveportal-login.cooper.edu/cgi-bin/login?cmd=logout")

    fi
    ;;
esac

if [[ $result == *"Login incorrect"* ]]; then
    echo "Login incorrect"
else
    msg=$(echo $result | grep -o '<b>.*</b>' | sed 's/\(<b>\|<\/b>\)//g')
    echo $msg
fi
