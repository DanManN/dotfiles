#!/usr/bin/env bash
XCOLORS_FILE=$HOME/.Xcolors

rm -f $XCOLORS_FILE
ln -s $HOME/.base16-xresources/xresources/base16-$BASE16_THEME.Xresources $XCOLORS_FILE
command -v xrdb >/dev/null 2>&1 && xrdb -merge $XCOLORS_FILE
i3-msg >/dev/null 2>&1 && i3-msg restart
echo 'awesome.restart()' | awesome-client >/dev/null 2>&1
