#!/usr/bin/env bash
ROFI_DIR=$HOME/.config/rofi
ROFI_THEME=$ROFI_DIR/base16.rasi

if [[ -d $ROFI_DIR ]]; then
	rm -f $ROFI_THEME
	ln -s $HOME/.base16-rofi/themes/base16-$BASE16_THEME.rasi $ROFI_THEME
fi

