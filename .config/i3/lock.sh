#!/usr/bin/env bash
#fortune=$(fortune -s -n 400 | sed -e 's/\x09/\x20\x20\x20\x20\x20/g')
#i3lock-fancy -t "$fortune" -- scrot -z &&
#echo "$fortune" > $HOME/.fortune
# i3lock -i $HOME/.config/i3/stargatelock2.png -r 229 -x || i3lock
$HOME/.config/i3/lockmulti.sh -a"\
	--radius=260 \
	--ring-width=32 \
	--indicator \
	--screen 2 \
	\
	--insidever-color=6f93bdff \
	--ringver-color=66ccccff \
	\
	--insidewrong-color=666770ff \
	--ringwrong-color=66ccccff \
	\
	--inside-color=00000000 \
	--ring-color=66ccccff \
	--line-color=00000000 \
	--separator-color=00000000 \
	\
	--keyhl-color=f9bf15ff \
	--bshl-color=ca0000ff \
	\
	--clock \
	--time-str='%H:%M:%S' \
	--date-str='%A, %m %Y' \
	--verif-text= \
	--wrong-text= \
	--noinput-text= \
	--lock-text= \
	--lockfailed-text='Lock Failed!' \
	--greeter-text= \
	--no-modkey-text"
	# --verif-color=ffffffff \
	# --wrong-color=ffffffff \
	# --time-color=ffffffff \
	# --date-color=ffffffff \
	# --layout-color=ffffffff \
