#!/usr/bin/env python
try:
    from openrazer.client import DeviceManager
    from polychromatic.preferences import get_device_state
    from polychromatic import preferences
    from polychromatic.common import set_lighting_effect
    device_manager = DeviceManager()
    keyboard = None
    for device in device_manager.devices:
        if (device.type == "keyboard"):
            keyboard = device

    if keyboard:
        device_manager.sync_effects = False
        serial = str(keyboard.serial)
        effect = get_device_state(serial,"main","effect")
        params = get_device_state(serial,"main","effect_params")
        set_lighting_effect(preferences, keyboard, "main", effect, params)
except:
    pass


